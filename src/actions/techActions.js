import {
  GET_TECHS,
  ADD_TECH,
  TECHS_ERROR,
  SET_LOADING,
} from './types';

// Get all techs from database
export const getTechs = () => async dispatch => {
  try {
    setLoading();

    const res = await fetch('/techs');
    const data = await res.json();

    dispatch({
      type: GET_TECHS,
      payload: data,
    });
  } catch (err) {
    dispatch({
      type: TECHS_ERROR,
      payload: err.response.statusText,
    });
  }
};

// Add tech to the database
export const addTech = tech => async dispatch => {
  try {
    setLoading();
    console.log(`TRYING`, tech);

    const res = await fetch('/techs', {
      method: 'POST',
      body: JSON.stringify(tech),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    console.log(`after await`, res);

    const data = await res.json();
    console.log(`after data`, data);

    dispatch({
      type: ADD_TECH,
      payload: data,
    });
  } catch (err) {
    console.log(`catch block`, err);
    dispatch({
      type: TECHS_ERROR,
      payload: err?.response?.statusText,
    });
  }
};

// Set loading true
export const setLoading = () => {
  return {
    type: SET_LOADING,
  };
};
