import React, { useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { getTechs } from '../../actions/techActions';
import Preloader from '../layout/Preloader';
import M from 'materialize-css/dist/js/materialize.min.js';
// import $ from 'jquery';

const TechSelectOptions = ({ tech: { techs, loading }, getTechs, setTech }) => {

  // TEST 2: "Add Log Modal Select a Technician" 
  // 1.2. Complete the `TechSelectOptions` component to list all the technicians in a dropdown list.
  // First get the technicians, check if the endpoint has finished loading. 
  // Then map the technicians from state to create the options list with `firstName` and `lastName` values. 

  useEffect(() => {
    getTechs();
    M.AutoInit();
    //eslint-disable-next-line
  }, []);

  // useEffect(() => {
  //   $(document).ready(function () {
  //     $('select').material_select();
  //   });
  // }, []);

  if (loading || techs === null) {
    return <Preloader />;
  }

  return (
    <div className="input-field col s12">
      <select
        onChange={(e) => { setTech(e.target.value) }}
        id='tech_sel'
      >
        <option
          value=""
          disabled
        >
          Select Technician
        </option>
        {techs
          .map(({ id, firstName, lastName }) => (
            <option key={id} value={firstName + " " + lastName}>
              {`${firstName} ${lastName}`}
            </option>
          ))}
      </select>
    </div>
  );
};

TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    getTechs: () => {
      dispatch(getTechs());
    },
  };
}

const mapStateToProps = state => ({
  tech: state.tech,
});

export default connect(mapStateToProps, mapDispatchToProps)(TechSelectOptions);
