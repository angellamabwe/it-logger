import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addTech } from '../../actions/techActions';
import M from 'materialize-css/dist/js/materialize.min.js';


const AddTechModal = (props) => {
  const initialValues = { firstName: "", lastName: "" };
  const [formValues, setFormValues] = useState(initialValues);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  }

  const onSubmit = () => {
    if (formValues.firstName === '' || formValues.lastName === '') {
      M.toast({ html: 'Please enter the first and last name' });
    } else {

      // TEST 1: "Create a new Technician" 
      // 4. Use the `addTech` redux action provided to save the new technician to the db.json "database".
      props.onSubmitForm(formValues);
      M.toast({ html: `${formValues.firstName} ${formValues.lastName} was added as a tech` });

      // TEST: "Create a new Technician" 
      // 5. Clear input fields after save
      setFormValues({ firstName: "", lastName: "" });
    }
  };

  return (
    <div id='add-tech-modal' className='modal' >
      <div className='modal-content'>
        <h4>New Technician</h4>

        <div className="row">
          <form className="col s12">
            <div className="row">
              <div className="input-field col s12">
                <input
                  id="first_name"
                  name="firstName"
                  type="text"
                  className="validate"
                  value={formValues.firstName}
                  onChange={handleChange}
                />
                <label htmlFor="first_name">First Name</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s12">
                <input
                  id="last_name"
                  name="lastName"
                  type="text"
                  className="validate"
                  value={formValues.lastName}
                  onChange={handleChange}
                />
                <label htmlFor="last_name">Last Name</label>
              </div>
            </div>
          </form>
        </div>

        {/* 
        TEST 1: "Create a new Technician" 
        1. Add inputs for First Name and Last Name.
        2. onChange store the `firstName` and `lastName` inputs in state.
        3. onSubmit the technician must be saved to the database.
        */}

      </div>
      <div className='modal-footer'>
        <a
          href='#!'
          onClick={onSubmit}
          className='modal-close waves-effect blue waves-light btn'
        >
          Enter
        </a>
      </div>
    </div >
  );
};

AddTechModal.propTypes = {
  onSubmitForm: PropTypes.func.isRequired,

};

function mapDispatchToProps(dispatch) {
  return {
    onSubmitForm: (formValues) => {
      dispatch(addTech(formValues));
    },
  };
}

export default connect(null, mapDispatchToProps)(AddTechModal);
